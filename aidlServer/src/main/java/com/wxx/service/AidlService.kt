package com.wxx.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.wxx.service.event.AcceptEvent
import org.greenrobot.eventbus.EventBus

class AidlService : Service() {
    private val TAG = AidlService::class.java.simpleName
    var listener :MyListener? = null
    private val aidl = object : MyAidl.Stub(){
        override fun calculate(num1: Float, num2: Float) {
            log("我是服务器:>>收到参数${num1}和$num2")
            listener?.sum("${num1 + num2}")
            EventBus.getDefault().post(AcceptEvent(num1, num2))
        }

        override fun addListener(l: MyListener?) {
            this@AidlService.listener = l
        }
    }

    override fun onBind(intent: Intent?): IBinder {
        return aidl
    }

    private fun log(str: String){
        Log.e(TAG, str)
    }
}