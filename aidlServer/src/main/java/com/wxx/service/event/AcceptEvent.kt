package com.wxx.service.event

/**
 * 本进程的Activity和Service通信的实体
 */
class AcceptEvent(var num1: Float, var num2: Float)