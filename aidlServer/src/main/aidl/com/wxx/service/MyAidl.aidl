// MyAidl.aidl
package com.wxx.service;
import com.wxx.service.MyListener;
// Declare any non-default types here with import statements

interface MyAidl {
    void calculate(float num1, float num2);
    void addListener(in MyListener l);
}
