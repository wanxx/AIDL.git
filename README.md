# AIDL

#### 项目介绍
Android使用AIDL进行进程间的通讯，进程A输入两个数，进程B求和后并返回给进程A


#### 安装教程
1. 在发行版标签页可下载直接编译好的app
2. 自行下载源码，里面有有两个module,代表两个app

#### 使用说明
1. 必须先启动aidlService app


#### 效果预览
![输入图片说明](https://images.gitee.com/uploads/images/2018/1107/160713_123182a7_938591.gif "预览")

#### 参与贡献

1. Fork 本项目
2. 新建 func_xxx 分支
3. 提交代码
4. 新建 Pull Request
